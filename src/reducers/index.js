// combining counter.js and user.js
import {combineReducers} from "redux";

import userReducer from "./user";
import counterReducer from "./counter";

const allReducers = combineReducers({
    // user: userReducer,
    // counter: counterReducer
    userReducer,
    counterReducer
});

export default allReducers;