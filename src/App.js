import React from "react"; 
import './App.css';
import {useSelector, useDispatch} from "react-redux";
import {increment, decrement, reset, addUser, removeUser} from "./actions"
//useSelector -> access state
//useDispatch-> dispatch action to the store ; reducer methord returns on that basis

const App = () =>{
  const count = useSelector(state => state.counterReducer)
  // const users = useSelector(state => state.userReducer)
  const dispatch = useDispatch();
  return (
    <div className="App">
     <h2>Counter</h2>
     <button onClick={() => dispatch(increment())}>+</button>
     {count}
     <button  onClick={() => dispatch(decrement())}>-</button>
     {/* <h2>User</h2> */}
    </div>
  );
}

export default App;
